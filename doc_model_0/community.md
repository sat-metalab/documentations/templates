*The community page is about connecting with other users.*

--------------------------------------------------------------------------------

# Community

Description of the community, overview of the CoC, [link to CoC].

To get in touch with the users community, you may want to visit:

- [chat]
- [mailing list]
- [meetups]
- [other !]

To get in touch with the core dev team, visit [link to contact.md].

Looking to contribute to the $PROJECT ? [link to CONTRIBUTING.md].

